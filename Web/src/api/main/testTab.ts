﻿import request from '/@/utils/request';
enum Api {
  AddTestTab = '/api/testTab/add',
  DeleteTestTab = '/api/testTab/delete',
  UpdateTestTab = '/api/testTab/update',
  PageTestTab = '/api/testTab/page',
}

// 增加TestTab
export const addTestTab = (params?: any) =>
	request({
		url: Api.AddTestTab,
		method: 'post',
		data: params,
	});

// 删除TestTab
export const deleteTestTab = (params?: any) => 
	request({
			url: Api.DeleteTestTab,
			method: 'post',
			data: params,
		});

// 编辑TestTab
export const updateTestTab = (params?: any) => 
	request({
			url: Api.UpdateTestTab,
			method: 'post',
			data: params,
		});

// 分页查询TestTab
export const pageTestTab = (params?: any) => 
	request({
			url: Api.PageTestTab,
			method: 'post',
			data: params,
		});


