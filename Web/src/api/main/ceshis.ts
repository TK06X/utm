﻿import request from '/@/utils/request';
enum Api {
  Addceshis = '/api/ceshis/add',
  Deleteceshis = '/api/ceshis/delete',
  Updateceshis = '/api/ceshis/update',
  Pageceshis = '/api/ceshis/page',
}

// 增加cses
export const addceshis = (params?: any) =>
	request({
		url: Api.Addceshis,
		method: 'post',
		data: params,
	});

// 删除cses
export const deleteceshis = (params?: any) => 
	request({
			url: Api.Deleteceshis,
			method: 'post',
			data: params,
		});

// 编辑cses
export const updateceshis = (params?: any) => 
	request({
			url: Api.Updateceshis,
			method: 'post',
			data: params,
		});

// 分页查询cses
export const pageceshis = (params?: any) => 
	request({
			url: Api.Pageceshis,
			method: 'post',
			data: params,
		});


