﻿import request from '/@/utils/request';
enum Api {
  AddATab = '/api/aTab/add',
  DeleteATab = '/api/aTab/delete',
  UpdateATab = '/api/aTab/update',
  PageATab = '/api/aTab/page',
}

// 增加456
export const addATab = (params?: any) =>
	request({
		url: Api.AddATab,
		method: 'post',
		data: params,
	});

// 删除456
export const deleteATab = (params?: any) => 
	request({
			url: Api.DeleteATab,
			method: 'post',
			data: params,
		});

// 编辑456
export const updateATab = (params?: any) => 
	request({
			url: Api.UpdateATab,
			method: 'post',
			data: params,
		});

// 分页查询456
export const pageATab = (params?: any) => 
	request({
			url: Api.PageATab,
			method: 'post',
			data: params,
		});


