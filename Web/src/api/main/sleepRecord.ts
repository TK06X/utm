﻿import request from '/@/utils/request';
enum Api {
  AddSleepRecord = '/api/sleepRecord/add',
  DeleteSleepRecord = '/api/sleepRecord/delete',
  UpdateSleepRecord = '/api/sleepRecord/update',
  PageSleepRecord = '/api/sleepRecord/page',
}

// 增加SleepRecord
export const addSleepRecord = (params?: any) =>
	request({
		url: Api.AddSleepRecord,
		method: 'post',
		data: params,
	});

// 删除SleepRecord
export const deleteSleepRecord = (params?: any) => 
	request({
			url: Api.DeleteSleepRecord,
			method: 'post',
			data: params,
		});

// 编辑SleepRecord
export const updateSleepRecord = (params?: any) => 
	request({
			url: Api.UpdateSleepRecord,
			method: 'post',
			data: params,
		});

// 分页查询SleepRecord
export const pageSleepRecord = (params?: any) => 
	request({
			url: Api.PageSleepRecord,
			method: 'post',
			data: params,
		});


