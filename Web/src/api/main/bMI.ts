﻿import request from '/@/utils/request';
enum Api {
  AddBMI = '/api/bMI/add',
  DeleteBMI = '/api/bMI/delete',
  UpdateBMI = '/api/bMI/update',
  PageBMI = '/api/bMI/page',
}

// 增加BMI
export const addBMI = (params?: any) =>
	request({
		url: Api.AddBMI,
		method: 'post',
		data: params,
	});

// 删除BMI
export const deleteBMI = (params?: any) => 
	request({
			url: Api.DeleteBMI,
			method: 'post',
			data: params,
		});

// 编辑BMI
export const updateBMI = (params?: any) => 
	request({
			url: Api.UpdateBMI,
			method: 'post',
			data: params,
		});

// 分页查询BMI
export const pageBMI = (params?: any) => 
	request({
			url: Api.PageBMI,
			method: 'post',
			data: params,
		});


