﻿import request from '/@/utils/request';

enum Api {
	AddTallyRecord = '/api/tallyRecord/add',
	DeleteTallyRecord = '/api/tallyRecord/delete',
	UpdateTallyRecord = '/api/tallyRecord/update',
	PageTallyRecord = '/api/tallyRecord/page',
}

// 增加账单
export const addTallyRecord = (params?: any) =>
	request({
		url: Api.AddTallyRecord,
		method: 'post',
		data: params,
	});

// 删除账单
export const deleteTallyRecord = (params?: any) =>
	request({
		url: Api.DeleteTallyRecord,
		method: 'post',
		data: params,
	});

// 编辑账单
export const updateTallyRecord = (params?: any) =>
	request({
		url: Api.UpdateTallyRecord,
		method: 'post',
		data: params,
	});

// 分页查询账单
export const pageTallyRecord = (params?: any) =>
	request({
		url: Api.PageTallyRecord,
		method: 'post',
		data: params,
	});
