﻿import request from '/@/utils/request';

enum Api {
	AddA_SleepRecord = '/api/sleepRecord/add',
	DeleteA_SleepRecord = '/api/sleepRecord/delete',
	UpdateA_SleepRecord = '/api/sleepRecord/update',
	PageA_SleepRecord = '/api/sleepRecord/page',
	monthSearch = '/api/sleepRecord/monthSearch',
}

// 月份查询
export const monthSearch = (params?: any) =>
	request({
		url: Api.monthSearch,
		method: 'post',
		data: params,
	});

// 增加Sleeps
export const addA_SleepRecord = (params?: any) =>
	request({
		url: Api.AddA_SleepRecord,
		method: 'post',
		data: params,
	});

// 删除Sleeps
export const deleteA_SleepRecord = (params?: any) =>
	request({
		url: Api.DeleteA_SleepRecord,
		method: 'post',
		data: params,
	});

// 编辑Sleeps
export const updateA_SleepRecord = (params?: any) =>
	request({
		url: Api.UpdateA_SleepRecord,
		method: 'post',
		data: params,
	});

// 分页查询Sleeps
export const pageA_SleepRecord = (params?: any) =>
	request({
		url: Api.PageA_SleepRecord,
		method: 'post',
		data: params,
	});
