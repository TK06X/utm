﻿import request from '/@/utils/request';

enum Api {
	AddWorkBook = '/api/workBook/add',
	DeleteWorkBook = '/api/workBook/delete',
	UpdateWorkBook = '/api/workBook/update',
	PageWorkBook = '/api/workBook/page',
}

// 词书管理
// 增加词书
export const addWorkBook = (params?: any) =>
	request({
		url: Api.AddWorkBook,
		method: 'post',
		data: params,
	});

// 删除词书
export const deleteWorkBook = (params?: any) =>
	request({
		url: Api.DeleteWorkBook,
		method: 'post',
		data: params,
	});

// 编辑词书
export const updateWorkBook = (params?: any) =>
	request({
		url: Api.UpdateWorkBook,
		method: 'post',
		data: params,
	});

// 分页查询词书
export const pageWorkBook = (params?: any) =>
	request({
		url: Api.PageWorkBook,
		method: 'post',
		data: params,
	});
