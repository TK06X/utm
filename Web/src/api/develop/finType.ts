﻿enum Api {
	AddFinType = '/api/finType/add',
	DeleteFinType = '/api/finType/delete',
	UpdateFinType = '/api/finType/update',
	PageFinType = '/api/finType/page',
}

// 增加收支分类
export const addFinType = (params?: any) =>
	request({
		url: Api.AddFinType,
		method: 'post',
		data: params,
	});

// 删除收支分类
export const deleteFinType = (params?: any) =>
	request({
		url: Api.DeleteFinType,
		method: 'post',
		data: params,
	});

// 编辑收支分类
export const updateFinType = (params?: any) =>
	request({
		url: Api.UpdateFinType,
		method: 'post',
		data: params,
	});

// 分页查询收支分类
export const pageFinType = (params?: any) =>
	request({
		url: Api.PageFinType,
		method: 'post',
		data: params,
	});
