﻿import request from '/@/utils/request';

enum Api {
	AddHabit = '/api/habit/add',
	DeleteHabit = '/api/habit/delete',
	UpdateHabit = '/api/habit/update',
	PageHabit = '/api/habit/page',
}

// 增加Habit
export const addHabit = (params?: any) =>
	request({
		url: Api.AddHabit,
		method: 'post',
		data: params,
	});

// 删除Habit
export const deleteHabit = (params?: any) =>
	request({
		url: Api.DeleteHabit,
		method: 'post',
		data: params,
	});

// 编辑Habit
export const updateHabit = (params?: any) =>
	request({
		url: Api.UpdateHabit,
		method: 'post',
		data: params,
	});

// 分页查询Habit
export const pageHabit = (params?: any) =>
	request({
		url: Api.PageHabit,
		method: 'post',
		data: params,
	});
