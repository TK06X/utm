﻿using Admin.NET.Application.Const;
using Admin.NET.Application.Entity;

namespace Admin.NET.Application.Service;


/// <summary>
/// BMI服务
/// </summary>
[ApiDescriptionSettings(ApplicationConst.GroupName, Order = 100)]
public class BMIService : IDynamicApiController, ITransient
{
    private readonly SqlSugarRepository<BMI> _rep;
    public BMIService(SqlSugarRepository<BMI> rep)
    {
        _rep = rep;
    }

    /// <summary>
    /// 分页查询BMI
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Page")]
    public async Task<SqlSugarPagedList<BMIOutput>> Page(BMIInput input)
    {
        var query= _rep.AsQueryable()
            .Select<BMIOutput>()
;
        if(input.NowTimeRange != null && input.NowTimeRange.Count >0)
        {
            DateTime? start= input.NowTimeRange[0]; 
            query = query.WhereIF(start.HasValue, u => u.NowTime > start);
            if (input.NowTimeRange.Count >1 && input.NowTimeRange[1].HasValue)
            {
                var end = input.NowTimeRange[1].Value.AddDays(1);
                query = query.Where(u => u.NowTime < end);
            }
        } 
        query = query.OrderBuilder(input, "", "CreateTime");
        return await query.ToPagedListAsync(input.Page, input.PageSize);
    }

    /// <summary>
    /// 增加BMI
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Add")]
    public async Task Add(AddBMIInput input)
    {
        var entity = input.Adapt<BMI>();
        await _rep.InsertAsync(entity);
    }

    /// <summary>
    /// 删除BMI                                                                                                                                                                                                                                                                                                                                                                                                                                               
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Delete")]
    public async Task Delete(DeleteBMIInput input)
    {
        var entity = await _rep.GetFirstAsync(u => u.Id == input.Id) ?? throw Oops.Oh(ErrorCodeEnum.D1002);
        // await _rep.FakeDeleteAsync(entity);   //假删除
       await _rep.DeleteAsync(entity);   //真删除
    }

    /// <summary>
    /// 更新BMI
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Update")]
    public async Task Update(UpdateBMIInput input)
    {
        var entity = input.Adapt<BMI>();
        await _rep.AsUpdateable(entity).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
    }

    /// <summary>
    /// 获取BMI
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "Detail")]
    public async Task<BMI> Get([FromQuery] QueryByIdBMIInput input)
    {
        return await _rep.GetFirstAsync(u => u.Id == input.Id);
    }

    /// <summary>
    /// 获取BMI列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "List")]
    public async Task<List<BMIOutput>> List([FromQuery] BMIInput input)
    {
        return await _rep.AsQueryable().Select<BMIOutput>().ToListAsync();
    }
 
}

