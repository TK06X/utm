﻿namespace Admin.NET.Application;

/// <summary>
/// BMI输出参数
/// </summary>
public class BMIOutput
{
    /// <summary>
    /// 主键Id
    /// </summary>
    public long Id { get; set; }
    
    /// <summary>
    /// 身高
    /// </summary>
    public decimal? Height { get; set; }
    
    /// <summary>
    /// 体重
    /// </summary>
    public decimal? Weight { get; set; }
    
    /// <summary>
    /// BMI
    /// </summary>
    public decimal? BmiComp { get; set; }
    
    /// <summary>
    /// 记录时间
    /// </summary>
    public DateTime NowTime { get; set; }
    
    }
 

