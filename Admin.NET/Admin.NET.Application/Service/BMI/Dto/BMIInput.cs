﻿using Admin.NET.Core;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application;

    /// <summary>
    /// BMI基础输入参数
    /// </summary>
    public class BMIBaseInput
    {
        /// <summary>
        /// 身高
        /// </summary>
        public virtual decimal? Height { get; set; }
        
        /// <summary>
        /// 体重
        /// </summary>
        public virtual decimal? Weight { get; set; }
        
        /// <summary>
        /// BMI
        /// </summary>
        public virtual decimal? BmiComp { get; set; }
        
        /// <summary>
        /// 记录时间
        /// </summary>
        public virtual DateTime NowTime { get; set; }
        
    }

    /// <summary>
    /// BMI分页查询输入参数
    /// </summary>
    public class BMIInput : BasePageInput
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        public string SearchKey { get; set; }

        /// <summary>
        /// 身高
        /// </summary>
        public decimal? Height { get; set; }
        
        /// <summary>
        /// 体重
        /// </summary>
        public decimal? Weight { get; set; }
        
        /// <summary>
        /// BMI
        /// </summary>
        public decimal? BmiComp { get; set; }
        
        /// <summary>
        /// 记录时间
        /// </summary>
        public DateTime NowTime { get; set; }
        
        /// <summary>
         /// 记录时间范围
         /// </summary>
         public List<DateTime?> NowTimeRange { get; set; } 
    }

    /// <summary>
    /// BMI增加输入参数
    /// </summary>
    public class AddBMIInput : BMIBaseInput
    {
        /// <summary>
        /// 记录时间
        /// </summary>
        [Required(ErrorMessage = "记录时间不能为空")]
        public override DateTime NowTime { get; set; }
        
    }

    /// <summary>
    /// BMI删除输入参数
    /// </summary>
    public class DeleteBMIInput : BaseIdInput
    {
    }

    /// <summary>
    /// BMI更新输入参数
    /// </summary>
    public class UpdateBMIInput : BMIBaseInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "主键Id不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// BMI主键查询输入参数
    /// </summary>
    public class QueryByIdBMIInput : DeleteBMIInput
    {

    }
