﻿using Admin.NET.Application.Const;
namespace Admin.NET.Application.Service;

/// <summary>
/// 英语词书服务
/// </summary>
[ApiDescriptionSettings(ApplicationConst.GroupName, Order = 100)]
public class WorkBookService : IDynamicApiController, ITransient
{
    private readonly SqlSugarRepository<WorkBook> _rep;
    public WorkBookService(SqlSugarRepository<WorkBook> rep)
    {
        _rep = rep;
    }

    /// <summary>
    /// 分页查询英语词书
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Page")]
    public async Task<SqlSugarPagedList<WorkBookOutput>> Page(WorkBookInput input)
    {
        var query= _rep.AsQueryable()
            .WhereIF(!string.IsNullOrWhiteSpace(input.SearchKey), u =>
                u.Name.Contains(input.SearchKey.Trim())
                || u.TypeLevel.Contains(input.SearchKey.Trim())
                || u.Comment.Contains(input.SearchKey.Trim())
            )
            .WhereIF(!string.IsNullOrWhiteSpace(input.Name), u => u.Name.Contains(input.Name.Trim()))
            .WhereIF(!string.IsNullOrWhiteSpace(input.TypeLevel), u => u.TypeLevel.Contains(input.TypeLevel.Trim()))
            .WhereIF(!string.IsNullOrWhiteSpace(input.Comment), u => u.Comment.Contains(input.Comment.Trim()))
            .WhereIF(input.WorkCount>0, u => u.WorkCount == input.WorkCount)
            .Select<WorkBookOutput>()
;
        query = query.OrderBuilder(input, "", "Id");
        return await query.ToPagedListAsync(input.Page, input.PageSize);
    }

    /// <summary>
    /// 增加英语词书
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Add")]
    public async Task Add(AddWorkBookInput input)
    {
        var entity = input.Adapt<WorkBook>();
        await _rep.InsertAsync(entity);
    }

    /// <summary>
    /// 删除英语词书
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Delete")]
    public async Task Delete(DeleteWorkBookInput input)
    {
        var entity = await _rep.GetFirstAsync(u => u.Id == input.Id) ?? throw Oops.Oh(ErrorCodeEnum.D1002);
        await _rep.DeleteAsync(entity);   //假删除
        //await _rep.DeleteAsync(entity);   //真删除
    }

    /// <summary>
    /// 更新英语词书
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Update")]
    public async Task Update(UpdateWorkBookInput input)
    {
        var entity = input.Adapt<WorkBook>();
        await _rep.AsUpdateable(entity).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
    }

    /// <summary>
    /// 获取英语词书
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "Detail")]
    public async Task<WorkBook> Get([FromQuery] QueryByIdWorkBookInput input)
    {
        return await _rep.GetFirstAsync(u => u.Id == input.Id);
    }

    /// <summary>
    /// 获取英语词书列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "List")]
    public async Task<List<WorkBookOutput>> List([FromQuery] WorkBookInput input)
    {
        return await _rep.AsQueryable().Select<WorkBookOutput>().ToListAsync();
    }





}

