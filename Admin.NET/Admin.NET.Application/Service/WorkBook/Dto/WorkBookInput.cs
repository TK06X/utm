﻿using Admin.NET.Core;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application;

    /// <summary>
    /// 英语词书基础输入参数
    /// </summary>
    public class WorkBookBaseInput
    {
        /// <summary>
        /// 词书
        /// </summary>
        public virtual string? Name { get; set; }
        
        /// <summary>
        /// 分类
        /// </summary>
        public virtual string? TypeLevel { get; set; }
        
        /// <summary>
        /// 其他
        /// </summary>
        public virtual string? Comment { get; set; }
        
        /// <summary>
        /// 数量
        /// </summary>
        public virtual int? WorkCount { get; set; }
        
    }

    /// <summary>
    /// 英语词书分页查询输入参数
    /// </summary>
    public class WorkBookInput : BasePageInput
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        public string SearchKey { get; set; }

        /// <summary>
        /// 词书
        /// </summary>
        public string? Name { get; set; }
        
        /// <summary>
        /// 分类
        /// </summary>
        public string? TypeLevel { get; set; }
        
        /// <summary>
        /// 其他
        /// </summary>
        public string? Comment { get; set; }
        
        /// <summary>
        /// 数量
        /// </summary>
        public int? WorkCount { get; set; }
        
    }

    /// <summary>
    /// 英语词书增加输入参数
    /// </summary>
    public class AddWorkBookInput : WorkBookBaseInput
    {
    }

    /// <summary>
    /// 英语词书删除输入参数
    /// </summary>
    public class DeleteWorkBookInput : BaseIdInput
    {
    }

    /// <summary>
    /// 英语词书更新输入参数
    /// </summary>
    public class UpdateWorkBookInput : WorkBookBaseInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "主键Id不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// 英语词书主键查询输入参数
    /// </summary>
    public class QueryByIdWorkBookInput : DeleteWorkBookInput
    {

    }
