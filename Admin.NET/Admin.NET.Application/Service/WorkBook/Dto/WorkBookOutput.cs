﻿namespace Admin.NET.Application;

/// <summary>
/// 英语词书输出参数
/// </summary>
public class WorkBookOutput
{
    /// <summary>
    /// 主键Id
    /// </summary>
    public long Id { get; set; }
    
    /// <summary>
    /// 词书
    /// </summary>
    public string? Name { get; set; }
    
    /// <summary>
    /// 分类
    /// </summary>
    public string? TypeLevel { get; set; }
    
    /// <summary>
    /// 其他
    /// </summary>
    public string? Comment { get; set; }
    
    /// <summary>
    /// 数量
    /// </summary>
    public int? WorkCount { get; set; }
    
    }
 

