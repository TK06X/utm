﻿using Admin.NET.Core;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application;

    /// <summary>
    /// 收支分类基础输入参数
    /// </summary>
    public class FinTypeBaseInput
    {
        /// <summary>
        /// 分类
        /// </summary>
        public virtual string? TypeRecord { get; set; }
        
        /// <summary>
        /// 收入支出
        /// </summary>
        public virtual int? InOrOut { get; set; }
        
    }

    /// <summary>
    /// 收支分类分页查询输入参数
    /// </summary>
    public class FinTypeInput : BasePageInput
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        public string SearchKey { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        public string? TypeRecord { get; set; }
        
        /// <summary>
        /// 收入支出
        /// </summary>
        public int? InOrOut { get; set; }
        
    }

    /// <summary>
    /// 收支分类增加输入参数
    /// </summary>
    public class AddFinTypeInput : FinTypeBaseInput
    {
    }

    /// <summary>
    /// 收支分类删除输入参数
    /// </summary>
    public class DeleteFinTypeInput : BaseIdInput
    {
    }

    /// <summary>
    /// 收支分类更新输入参数
    /// </summary>
    public class UpdateFinTypeInput : FinTypeBaseInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "主键Id不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// 收支分类主键查询输入参数
    /// </summary>
    public class QueryByIdFinTypeInput : DeleteFinTypeInput
    {

    }
