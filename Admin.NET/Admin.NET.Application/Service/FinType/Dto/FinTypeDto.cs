﻿namespace Admin.NET.Application;

    /// <summary>
    /// 收支分类输出参数
    /// </summary>
    public class FinTypeDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 分类
        /// </summary>
        public string? TypeRecord { get; set; }
        
        /// <summary>
        /// 收入支出
        /// </summary>
        public int? InOrOut { get; set; }
        
    }
