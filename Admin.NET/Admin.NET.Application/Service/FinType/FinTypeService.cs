﻿using Admin.NET.Application.Const;
namespace Admin.NET.Application;
/// <summary>
/// 收支分类服务
/// </summary>
[ApiDescriptionSettings(ApplicationConst.GroupName, Order = 100)]
public class FinTypeService : IDynamicApiController, ITransient
{
    private readonly SqlSugarRepository<FinType> _rep;
    public FinTypeService(SqlSugarRepository<FinType> rep)
    {
        _rep = rep;
    }

    /// <summary>
    /// 分页查询收支分类
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Page")]
    public async Task<SqlSugarPagedList<FinTypeOutput>> Page(FinTypeInput input)
    {
        var query= _rep.AsQueryable()
            .WhereIF(!string.IsNullOrWhiteSpace(input.SearchKey), u =>
                u.TypeRecord.Contains(input.SearchKey.Trim())
            )
            .WhereIF(!string.IsNullOrWhiteSpace(input.TypeRecord), u => u.TypeRecord.Contains(input.TypeRecord.Trim()))
            .WhereIF(input.InOrOut>0, u => u.InOrOut == input.InOrOut)
            .Select<FinTypeOutput>()
;
        query = query.OrderBuilder(input, "", "CreateTime");
        return await query.ToPagedListAsync(input.Page, input.PageSize);
    }

    /// <summary>
    /// 增加收支分类
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Add")]
    public async Task Add(AddFinTypeInput input)
    {
        var entity = input.Adapt<FinType>();
        await _rep.InsertAsync(entity);
    }

    /// <summary>
    /// 删除收支分类
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Delete")]
    public async Task Delete(DeleteFinTypeInput input)
    {
        var entity = await _rep.GetFirstAsync(u => u.Id == input.Id) ?? throw Oops.Oh(ErrorCodeEnum.D1002);
        await _rep.FakeDeleteAsync(entity);   //假删除
        //await _rep.DeleteAsync(entity);   //真删除
    }

    /// <summary>
    /// 更新收支分类
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Update")]
    public async Task Update(UpdateFinTypeInput input)
    {
        var entity = input.Adapt<FinType>();
        await _rep.AsUpdateable(entity).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
    }

    /// <summary>
    /// 获取收支分类
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "Detail")]
    public async Task<FinType> Get([FromQuery] QueryByIdFinTypeInput input)
    {
        return await _rep.GetFirstAsync(u => u.Id == input.Id);
    }

    /// <summary>
    /// 获取收支分类列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "List")]
    public async Task<List<FinTypeOutput>> List([FromQuery] FinTypeInput input)
    {
        return await _rep.AsQueryable().Select<FinTypeOutput>().ToListAsync();
    }





}

