﻿using Admin.NET.Application.Entity;
using Minio.DataModel;

namespace Admin.NET.Application.Dto;

public class MonthSearchOutPut : SleepRecord
{
    /// <summary>
    /// 测试
    /// </summary>
    public long? durationNew { get; set; }
}

/// <summary>
/// SleepRecord输出参数
/// </summary>
public class SleepRecordOutput
{
    /// <summary>
    /// 主键Id
    /// </summary>
    public long Id { get; set; }

    /// <summary>
    /// 用户id
    /// </summary>
    public string? UserId { get; set; }

    /// <summary>
    /// 开始时间
    /// </summary>
    public DateTime? StartTime { get; set; }

    /// <summary>
    /// 结束时间
    /// </summary>
    public DateTime? EndTime { get; set; }

    /// <summary>
    /// 持续时间
    /// </summary>
    public long? Duration { get; set; }

    /// <summary>
    /// 状态-良好、一般、失眠等
    /// </summary>
    public SleepStateEnum State { get; set; }

    /// <summary>
    /// 创建者姓名
    /// </summary>
    public string? CreateUserName { get; set; }

    /// <summary>
    /// 修改者姓名
    /// </summary>
    public string? UpdateUserName { get; set; }
}