﻿namespace Admin.NET.Application;

    /// <summary>
    /// SleepRecord输出参数
    /// </summary>
    public class SleepRecordDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 用户id
        /// </summary>
        public string? UserId { get; set; }
        
        /// <summary>
        /// 开始时间
        /// </summary>
        public DateTime? StartTime { get; set; }
        
        /// <summary>
        /// 结束时间
        /// </summary>
        public DateTime? EndTime { get; set; }
        
        /// <summary>
        /// 持续时间
        /// </summary>
        public TimeSpan? Duration { get; set; }
        
        /// <summary>
        /// 状态-良好、一般、失眠等
        /// </summary>
        public SleepStateEnum State { get; set; }
        
     
    }
