﻿using Admin.NET.Core;
using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace Admin.NET.Application;

/// <summary>
/// SleepRecord基础输入参数
/// </summary>
public class SleepRecordBaseInput
{
    /// <summary>
    /// 用户id
    /// </summary>
    public virtual string? UserId { get; set; }

    /// <summary>
    /// 开始时间
    /// </summary>
    [Required]
    public virtual DateTime? StartTime { get; set; }

    /// <summary>
    /// NowTime
    /// </summary>
    public virtual DateTime? NowTime { get; set; }

    /// <summary>
    /// 结束时间
    /// </summary>
    public virtual DateTime? EndTime { get; set; }

    /// <summary>
    /// 持续时间
    /// </summary>
    public virtual TimeSpan? Duration { get; set; }

    /// <summary>
    /// 状态-良好、一般、失眠等
    /// </summary>
    public virtual SleepStateEnum State { get; set; }
}

/// <summary>
/// 输入日期年月
/// </summary>
public class MonthSearchInput
{
    /// <summary>
    /// 年
    /// </summary>
    [Required(ErrorMessage = "年不能为空")]
    public int? Year { get; set; }

    /// <summary>
    /// 月
    /// </summary>
    [Required(ErrorMessage = "月不能为空")]
    public int? Month { get; set; }
}

/// <summary>
/// SleepRecord分页查询输入参数
/// </summary>
public class SleepRecordInput : BasePageInput
{
    /// <summary>
    /// 关键字查询
    /// </summary>
    public string SearchKey { get; set; }

    /// <summary>
    /// 年
    /// </summary>
    public int? Year { get; set; }

    /// <summary>
    /// 月
    /// </summary>
    public int? Month { get; set; }

    /// <summary>
    /// 开始时间
    /// </summary>
    public DateTime? StartTime { get; set; }

    /// <summary>
    /// 开始时间范围
    /// </summary>
    public List<DateTime?> StartTimeRange { get; set; }

    /// <summary>
    /// 结束时间
    /// </summary>
    public DateTime? EndTime { get; set; }

    /// <summary>
    /// 结束时间范围
    /// </summary>
    public List<DateTime?> EndTimeRange { get; set; }

    /// <summary>
    /// 持续时间
    /// </summary>
    public int? Duration { get; set; }

    /// <summary>
    /// 状态 
    /// </summary>
    public int? State { get; set; }
}

/// <summary>
/// SleepRecord增加输入参数
/// </summary>
public class AddSleepRecordInput : SleepRecordBaseInput
{
    /// <summary>
    /// TB
    /// </summary>
    [Required(ErrorMessage = "DateTime不能为空")]
    public override DateTime? NowTime { get; set; }

    /// <summary>
    /// TB
    /// </summary>
    [Required(ErrorMessage = "StartTime不能为空")]
    public override DateTime? StartTime { get; set; }

    /// <summary>
    /// TB
    /// </summary>
    [Required(ErrorMessage = "EndTime不能为空")]
    public override DateTime? EndTime { get; set; }
}

/// <summary>
/// SleepRecord删除输入参数
/// </summary>
public class DeleteSleepRecordInput : BaseIdInput
{
}

/// <summary>
/// SleepRecord更新输入参数
/// </summary>
public class UpdateSleepRecordInput : SleepRecordBaseInput
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [Required(ErrorMessage = "主键Id不能为空")]
    public long Id { get; set; }
}

/// <summary>
/// SleepRecord主键查询输入参数
/// </summary>
public class QueryByIdSleepRecordInput : DeleteSleepRecordInput
{
}