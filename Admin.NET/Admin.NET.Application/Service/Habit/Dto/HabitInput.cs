﻿using Admin.NET.Core;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application;

    /// <summary>
    /// Habit基础输入参数
    /// </summary>
    public class HabitBaseInput
    {
        /// <summary>
        /// 名称
        /// </summary>
        public virtual string? Name { get; set; }
        
        /// <summary>
        /// 持续时间(天)
        /// </summary>
        public virtual int? Duration { get; set; }
        
        /// <summary>
        /// 创建者姓名
        /// </summary>
        public virtual string? CreateUserName { get; set; }
        
        /// <summary>
        /// 修改者姓名
        /// </summary>
        public virtual string? UpdateUserName { get; set; }
        
    }

    /// <summary>
    /// Habit分页查询输入参数
    /// </summary>
    public class HabitInput : BasePageInput
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        public string SearchKey { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string? Name { get; set; }
        
        /// <summary>
        /// 持续时间(天)
        /// </summary>
        public int? Duration { get; set; }
        
    }

    /// <summary>
    /// Habit增加输入参数
    /// </summary>
    public class AddHabitInput : HabitBaseInput
    {
    }

    /// <summary>
    /// Habit删除输入参数
    /// </summary>
    public class DeleteHabitInput : BaseIdInput
    {
    }

    /// <summary>
    /// Habit更新输入参数
    /// </summary>
    public class UpdateHabitInput : HabitBaseInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "主键Id不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// Habit主键查询输入参数
    /// </summary>
    public class QueryByIdHabitInput : DeleteHabitInput
    {

    }
