﻿namespace Admin.NET.Application;

    /// <summary>
    /// 账单输出参数
    /// </summary>
    public class TallyRecordDto
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// 记录时间
        /// </summary>
        public DateTime? RecordTime { get; set; }
        
        /// <summary>
        /// 收入支出
        /// </summary>
        public InOrOut InOrOut { get; set; }
        
        /// <summary>
        /// 分类
        /// </summary>
        public string? TypeRecord { get; set; }
        
        /// <summary>
        /// 金额
        /// </summary>
        public decimal? Money { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public string? Comment { get; set; }
        
        /// <summary>
        /// 备用A
        /// </summary>
        public string? BackUpA { get; set; }
        
        /// <summary>
        /// 备用B
        /// </summary>
        public string? BackUpB { get; set; }
        
        /// <summary>
        /// 备用C
        /// </summary>
        public string? BackUpC { get; set; }
        
        /// <summary>
        /// 创建者姓名
        /// </summary>
        public string? CreateUserName { get; set; }
        
        /// <summary>
        /// 修改者姓名
        /// </summary>
        public string? UpdateUserName { get; set; }
        
    }
