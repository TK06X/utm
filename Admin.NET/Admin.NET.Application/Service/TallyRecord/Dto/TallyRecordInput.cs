﻿using Admin.NET.Core;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application;

    /// <summary>
    /// 账单基础输入参数
    /// </summary>
    public class TallyRecordBaseInput
    {
        /// <summary>
        /// 记录时间
        /// </summary>
        public virtual DateTime? RecordTime { get; set; }
        
        /// <summary>
        /// 收入支出
        /// </summary>
        public virtual InOrOut InOrOut { get; set; }
        
        /// <summary>
        /// 分类
        /// </summary>
        public virtual string? TypeRecord { get; set; }
        
        /// <summary>
        /// 金额
        /// </summary>
        public virtual decimal? Money { get; set; }
        
        /// <summary>
        /// 备注
        /// </summary>
        public virtual string? Comment { get; set; }
        
        /// <summary>
        /// 备用A
        /// </summary>
        public virtual string? BackUpA { get; set; }
        
        /// <summary>
        /// 备用B
        /// </summary>
        public virtual string? BackUpB { get; set; }
        
        /// <summary>
        /// 备用C
        /// </summary>
        public virtual string? BackUpC { get; set; }
        
        /// <summary>
        /// 创建者姓名
        /// </summary>
        public virtual string? CreateUserName { get; set; }
        
        /// <summary>
        /// 修改者姓名
        /// </summary>
        public virtual string? UpdateUserName { get; set; }
        
    }

    /// <summary>
    /// 账单分页查询输入参数
    /// </summary>
    public class TallyRecordInput : BasePageInput
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        public string SearchKey { get; set; }

        /// <summary>
        /// 记录时间
        /// </summary>
        public DateTime? RecordTime { get; set; }
        
        /// <summary>
         /// 记录时间范围
         /// </summary>
         public List<DateTime?> RecordTimeRange { get; set; } 
        /// <summary>
        /// 收入支出
        /// </summary>
        public InOrOut InOrOut { get; set; }
        
        /// <summary>
        /// 分类
        /// </summary>
        public string? TypeRecord { get; set; }
        
        /// <summary>
        /// 金额
        /// </summary>
        public decimal? Money { get; set; }
        
    }

    /// <summary>
    /// 账单增加输入参数
    /// </summary>
    public class AddTallyRecordInput : TallyRecordBaseInput
    {
    }

    /// <summary>
    /// 账单删除输入参数
    /// </summary>
    public class DeleteTallyRecordInput : BaseIdInput
    {
    }

    /// <summary>
    /// 账单更新输入参数
    /// </summary>
    public class UpdateTallyRecordInput : TallyRecordBaseInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "主键Id不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// 账单主键查询输入参数
    /// </summary>
    public class QueryByIdTallyRecordInput : DeleteTallyRecordInput
    {

    }
