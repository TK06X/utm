﻿using Admin.NET.Application.Const;
namespace Admin.NET.Application;
/// <summary>
/// 账单服务
/// </summary>
[ApiDescriptionSettings(ApplicationConst.GroupName, Order = 100)]
public class TallyRecordService : IDynamicApiController, ITransient
{
    private readonly SqlSugarRepository<TallyRecord> _rep;
    public TallyRecordService(SqlSugarRepository<TallyRecord> rep)
    {
        _rep = rep;
    }

    /// <summary>
    /// 分页查询账单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Page")]
    public async Task<SqlSugarPagedList<TallyRecordOutput>> Page(TallyRecordInput input)
    {
        var query= _rep.AsQueryable()
            .WhereIF(!string.IsNullOrWhiteSpace(input.SearchKey), u =>
                u.TypeRecord.Contains(input.SearchKey.Trim())
            )
            .WhereIF(!string.IsNullOrWhiteSpace(input.TypeRecord), u => u.TypeRecord.Contains(input.TypeRecord.Trim()))
            .Select<TallyRecordOutput>()
;
        if(input.RecordTimeRange != null && input.RecordTimeRange.Count >0)
        {
            DateTime? start= input.RecordTimeRange[0]; 
            query = query.WhereIF(start.HasValue, u => u.RecordTime > start);
            if (input.RecordTimeRange.Count >1 && input.RecordTimeRange[1].HasValue)
            {
                var end = input.RecordTimeRange[1].Value.AddDays(1);
                query = query.Where(u => u.RecordTime < end);
            }
        } 
        query = query.OrderBuilder(input, "", "CreateTime");
        return await query.ToPagedListAsync(input.Page, input.PageSize);
    }

    /// <summary>
    /// 增加账单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Add")]
    public async Task Add(AddTallyRecordInput input)
    {
        var entity = input.Adapt<TallyRecord>();
        await _rep.InsertAsync(entity);
    }

    /// <summary>
    /// 删除账单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Delete")]
    public async Task Delete(DeleteTallyRecordInput input)
    {
        var entity = await _rep.GetFirstAsync(u => u.Id == input.Id) ?? throw Oops.Oh(ErrorCodeEnum.D1002);
        await _rep.FakeDeleteAsync(entity);   //假删除
        //await _rep.DeleteAsync(entity);   //真删除
    }

    /// <summary>
    /// 更新账单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Update")]
    public async Task Update(UpdateTallyRecordInput input)
    {
        var entity = input.Adapt<TallyRecord>();
        await _rep.AsUpdateable(entity).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
    }

    /// <summary>
    /// 获取账单
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "Detail")]
    public async Task<TallyRecord> Get([FromQuery] QueryByIdTallyRecordInput input)
    {
        return await _rep.GetFirstAsync(u => u.Id == input.Id);
    }

    /// <summary>
    /// 获取账单列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "List")]
    public async Task<List<TallyRecordOutput>> List([FromQuery] TallyRecordInput input)
    {
        return await _rep.AsQueryable().Select<TallyRecordOutput>().ToListAsync();
    }





}

