﻿using Admin.NET.Application.Const;
using Admin.NET.Application.Entity;

namespace Admin.NET.Application.Service;

/// <summary>
/// TestTab服务
/// </summary>
[ApiDescriptionSettings(ApplicationConst.GroupName, Order = 100)]
public class TestTabService : IDynamicApiController, ITransient
{
    private readonly SqlSugarRepository<TestTab> _rep;
    public TestTabService(SqlSugarRepository<TestTab> rep)
    {
        _rep = rep;
    }

    /// <summary>
    /// 分页查询TestTab
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Page")]
    public async Task<SqlSugarPagedList<TestTabOutput>> Page(TestTabInput input)
    {
        var query= _rep.AsQueryable()
            .WhereIF(!string.IsNullOrWhiteSpace(input.SearchKey), u =>
                u.TBchar.Contains(input.SearchKey.Trim())
            )
            .WhereIF(!string.IsNullOrWhiteSpace(input.TBchar), u => u.TBchar.Contains(input.TBchar.Trim()))
            .WhereIF(input.TCint>0, u => u.TCint == input.TCint)
            .Select<TestTabOutput>()
;
        query = query.OrderBuilder(input, "", "CreateTime");
        return await query.ToPagedListAsync(input.Page, input.PageSize);
    }

    /// <summary>
    /// 增加TestTab
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Add")]
    public async Task Add(AddTestTabInput input)
    {
        var entity = input.Adapt<TestTab>();
        await _rep.InsertAsync(entity);
    }

    /// <summary>
    /// 删除TestTab
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Delete")]
    public async Task Delete(DeleteTestTabInput input)
    {
        var entity = await _rep.GetFirstAsync(u => u.Id == input.Id) ?? throw Oops.Oh(ErrorCodeEnum.D1002);
        await _rep.FakeDeleteAsync(entity);   //假删除
        //await _rep.DeleteAsync(entity);   //真删除
    }

    /// <summary>
    /// 更新TestTab
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpPost]
    [ApiDescriptionSettings(Name = "Update")]
    public async Task Update(UpdateTestTabInput input)
    {
        var entity = input.Adapt<TestTab>();
        await _rep.AsUpdateable(entity).IgnoreColumns(ignoreAllNullColumns: true).ExecuteCommandAsync();
    }

    /// <summary>
    /// 获取TestTab
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "Detail")]
    public async Task<TestTab> Get([FromQuery] QueryByIdTestTabInput input)
    {
        return await _rep.GetFirstAsync(u => u.Id == input.Id);
    }

    /// <summary>
    /// 获取TestTab列表
    /// </summary>
    /// <param name="input"></param>
    /// <returns></returns>
    [HttpGet]
    [ApiDescriptionSettings(Name = "List")]
    public async Task<List<TestTabOutput>> List([FromQuery] TestTabInput input)
    {
        return await _rep.AsQueryable().Select<TestTabOutput>().ToListAsync();
    }





}

