﻿namespace Admin.NET.Application;

/// <summary>
/// TestTab输出参数
/// </summary>
public class TestTabOutput
{
    /// <summary>
    /// 主键Id
    /// </summary>
    public long Id { get; set; }
    
    /// <summary>
    /// TA
    /// </summary>
    public YesNoEnum TAvar { get; set; }
    
    /// <summary>
    /// TB
    /// </summary>
    public string? TBchar { get; set; }
    
    /// <summary>
    /// TC
    /// </summary>
    public int? TCint { get; set; }
    
    /// <summary>
    /// TDdatetime
    /// </summary>
    public DateTime? TDdatetime { get; set; }
    
    /// <summary>
    /// TEtimestamp
    /// </summary>
    public string? TEtimestamp { get; set; }
    
    /// <summary>
    /// 创建者姓名
    /// </summary>
    public string? CreateUserName { get; set; }
    
    }
 

