﻿using Admin.NET.Core;
using System.ComponentModel.DataAnnotations;

namespace Admin.NET.Application;

    /// <summary>
    /// TestTab基础输入参数
    /// </summary>
    public class TestTabBaseInput
    {
        /// <summary>
        /// TA
        /// </summary>
        public virtual YesNoEnum TAvar { get; set; }
        
        /// <summary>
        /// TB
        /// </summary>
        public virtual string? TBchar { get; set; }
        
        /// <summary>
        /// TC
        /// </summary>
        public virtual int? TCint { get; set; }
        
        /// <summary>
        /// TDdatetime
        /// </summary>
        public virtual DateTime? TDdatetime { get; set; }
        
        /// <summary>
        /// TEtimestamp
        /// </summary>
        public virtual string? TEtimestamp { get; set; }
        
        /// <summary>
        /// 创建者姓名
        /// </summary>
        public virtual string? CreateUserName { get; set; }
        
    }

    /// <summary>
    /// TestTab分页查询输入参数
    /// </summary>
    public class TestTabInput : BasePageInput
    {
        /// <summary>
        /// 关键字查询
        /// </summary>
        public string SearchKey { get; set; }

        /// <summary>
        /// TA
        /// </summary>
        public YesNoEnum TAvar { get; set; }
        
        /// <summary>
        /// TB
        /// </summary>
        public string? TBchar { get; set; }
        
        /// <summary>
        /// TC
        /// </summary>
        public int? TCint { get; set; }
        
    }

    /// <summary>
    /// TestTab增加输入参数
    /// </summary>
    public class AddTestTabInput : TestTabBaseInput
    {
        /// <summary>
        /// TA
        /// </summary>
        [Required(ErrorMessage = "TA不能为空")]
        public override YesNoEnum TAvar { get; set; }
        
        /// <summary>
        /// TB
        /// </summary>
        [Required(ErrorMessage = "TB不能为空")]
        public override string? TBchar { get; set; }
        
        /// <summary>
        /// TC
        /// </summary>
        [Required(ErrorMessage = "TC不能为空")]
        public override int? TCint { get; set; }
        
    }

    /// <summary>
    /// TestTab删除输入参数
    /// </summary>
    public class DeleteTestTabInput : BaseIdInput
    {
    }

    /// <summary>
    /// TestTab更新输入参数
    /// </summary>
    public class UpdateTestTabInput : TestTabBaseInput
    {
        /// <summary>
        /// 主键Id
        /// </summary>
        [Required(ErrorMessage = "主键Id不能为空")]
        public long Id { get; set; }
        
    }

    /// <summary>
    /// TestTab主键查询输入参数
    /// </summary>
    public class QueryByIdTestTabInput : DeleteTestTabInput
    {

    }
