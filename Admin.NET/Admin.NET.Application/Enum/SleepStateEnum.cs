﻿namespace Admin.NET.Application;

/// <summary>
/// 睡眠状态枚举
/// </summary>
[Description("睡眠状态枚举")]
public enum SleepStateEnum
{
    /// <summary>
    /// 失眠
    /// </summary>
    [Description("缺少")]
    Agrypnia = 1,
    
    /// <summary>
    /// 一般
    /// </summary>
    [Description("一般")]
    Common = 2,

    /// <summary>
    /// 良好
    /// </summary>
    [Description("良好")]
    Fine = 3
}