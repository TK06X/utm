﻿namespace Admin.NET.Application;

/// <summary>
/// 收入支出枚举
/// </summary>
[Description("收入支出枚举")]
public enum InOrOut
{
    /// <summary>
    /// 收入
    /// </summary>
    [Description("收入")]
    InComing = 1,
    
    /// <summary>
    /// 支出
    /// </summary>
    [Description("支出")]
    Outgoing = 2,
}