﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 习惯
/// </summary>
[SugarTable("Habit","习惯")]
public class Habit  : EntityBase
{
    /// <summary>
    /// 名称
    /// </summary>
    [SugarColumn(ColumnName = "Name", ColumnDescription = "名称", Length = 32)]
    public string? Name { get; set; }
    
    /// <summary>
    /// 持续时间(天)
    /// </summary>
    [SugarColumn(ColumnName = "Duration", ColumnDescription = "持续时间(天)")]
    public int? Duration { get; set; }
    
    /// <summary>
    /// 创建者姓名
    /// </summary>
    [SugarColumn(ColumnName = "CreateUserName", ColumnDescription = "创建者姓名", Length = 64)]
    public string? CreateUserName { get; set; }
    
    /// <summary>
    /// 修改者姓名
    /// </summary>
    [SugarColumn(ColumnName = "UpdateUserName", ColumnDescription = "修改者姓名", Length = 64)]
    public string? UpdateUserName { get; set; }
    
}
