﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 睡眠记录表
/// </summary>
[SugarTable("SleepRecord","睡眠记录表")]
public class SleepRecord  : EntityBase
{
    /// <summary>
    /// 开始时间
    /// </summary>
    [SugarColumn(ColumnName = "StartTime", ColumnDescription = "开始时间")]
    public DateTime? StartTime { get; set; }
    
    /// <summary>
    /// 结束时间
    /// </summary>
    [SugarColumn(ColumnName = "EndTime", ColumnDescription = "结束时间")]
    public DateTime? EndTime { get; set; }
    
    /// <summary>
    /// 用户id
    /// </summary>
    [SugarColumn(ColumnName = "UserId", ColumnDescription = "用户id", Length = 32)]
    public string? UserId { get; set; }
    
    /// <summary>
    /// 持续时间
    /// </summary>
    [SugarColumn(ColumnName = "Duration", ColumnDescription = "持续时间")]
    public TimeSpan? Duration { get; set; }
    
    /// <summary>
    /// 状态
    /// </summary>
    [SugarColumn(ColumnName = "State", ColumnDescription = "状态")]
    public int? State { get; set; }
    
    /// <summary>
    /// 当条时刻
    /// </summary>
    [Required]
    [SugarColumn(ColumnName = "NowTime", ColumnDescription = "当条时刻")]
    public DateTime NowTime { get; set; }
    
}
