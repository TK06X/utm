﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 英语单词表
/// </summary>
[SugarTable("EnglishWork","英语单词表")]
public class EnglishWork  : EntityBase
{
    /// <summary>
    /// 英文
    /// </summary>
    [SugarColumn(ColumnName = "EN", ColumnDescription = "英文", Length = 32)]
    public string? EN { get; set; }
    
    /// <summary>
    /// 中文
    /// </summary>
    [SugarColumn(ColumnName = "CN", ColumnDescription = "中文", Length = 32)]
    public string? CN { get; set; }
    
    /// <summary>
    /// 音标
    /// </summary>
    [SugarColumn(ColumnName = "SoundMark", ColumnDescription = "音标", Length = 32)]
    public string? SoundMark { get; set; }
    
    /// <summary>
    /// 词书
    /// </summary>
    [Required]
    [SugarColumn(ColumnName = "WorkBook", ColumnDescription = "词书")]
    public int WorkBook { get; set; }
    
}
