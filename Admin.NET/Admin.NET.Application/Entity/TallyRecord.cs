﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 收入支出记录
/// </summary>
[SugarTable("TallyRecord","收入支出记录")]
public class TallyRecord  : EntityBase
{
    /// <summary>
    /// 记录时间
    /// </summary>
    [SugarColumn(ColumnName = "RecordTime", ColumnDescription = "记录时间")]
    public DateTime? RecordTime { get; set; }
    
    /// <summary>
    /// 收入支出
    /// </summary>
    [SugarColumn(ColumnName = "InOrOut", ColumnDescription = "收入支出")]
    public int? InOrOut { get; set; }
    
    /// <summary>
    /// 分类
    /// </summary>
    [SugarColumn(ColumnName = "TypeRecord", ColumnDescription = "分类", Length = 32)]
    public string? TypeRecord { get; set; }
    
    /// <summary>
    /// 金额
    /// </summary>
    [SugarColumn(ColumnName = "Money", ColumnDescription = "金额", Length = 8, DecimalDigits=4 )]
    public decimal? Money { get; set; }
    
    /// <summary>
    /// 备注
    /// </summary>
    [SugarColumn(ColumnName = "Comment", ColumnDescription = "备注", Length = 32)]
    public string? Comment { get; set; }
    
    /// <summary>
    /// 备用A
    /// </summary>
    [SugarColumn(ColumnName = "BackUpA", ColumnDescription = "备用A", Length = 32)]
    public string? BackUpA { get; set; }
    
    /// <summary>
    /// 备用B
    /// </summary>
    [SugarColumn(ColumnName = "BackUpB", ColumnDescription = "备用B", Length = 32)]
    public string? BackUpB { get; set; }
    
    /// <summary>
    /// 备用C
    /// </summary>
    [SugarColumn(ColumnName = "BackUpC", ColumnDescription = "备用C", Length = 32)]
    public string? BackUpC { get; set; }
   
}
