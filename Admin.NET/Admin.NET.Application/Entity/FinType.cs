﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 收入支出分类
/// </summary>
[SugarTable("FinType","收入支出分类")]
public class FinType  : EntityBase
{
    /// <summary>
    /// 分类
    /// </summary>
    [SugarColumn(ColumnName = "TypeRecord", ColumnDescription = "分类", Length = 32)]
    public string? TypeRecord { get; set; }
    
    /// <summary>
    /// 收入支出
    /// </summary>
    [SugarColumn(ColumnName = "InOrOut", ColumnDescription = "收入支出")]
    public int? InOrOut { get; set; }
    
}
