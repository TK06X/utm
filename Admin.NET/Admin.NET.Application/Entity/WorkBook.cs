﻿namespace Admin.NET.Application.Entity;

/// <summary>
/// 词书表
/// </summary>
[SugarTable("WorkBook","词书表")]
public class WorkBook 
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [Required]
    [SugarColumn(ColumnName = "Id", IsIdentity = false, ColumnDescription = "主键Id", IsPrimaryKey = true)]
    public long Id { get; set; }
    
    /// <summary>
    /// 词书
    /// </summary>
    [SugarColumn(ColumnName = "Name", ColumnDescription = "词书", Length = 32)]
    public string? Name { get; set; }
    
    /// <summary>
    /// 分类
    /// </summary>
    [SugarColumn(ColumnName = "TypeLevel", ColumnDescription = "分类", Length = 32)]
    public string? TypeLevel { get; set; }
    
    /// <summary>
    /// 其他
    /// </summary>
    [SugarColumn(ColumnName = "Comment", ColumnDescription = "其他", Length = 32)]
    public string? Comment { get; set; }
    
    /// <summary>
    /// 数量
    /// </summary>
    [SugarColumn(ColumnName = "WorkCount", ColumnDescription = "数量")]
    public int? WorkCount { get; set; }
    
}
