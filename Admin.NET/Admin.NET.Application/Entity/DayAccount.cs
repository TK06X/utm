﻿namespace Admin.NET.Application.Entity;

/// <summary>
/// 流水账
/// </summary>
[SugarTable("DayAccount","流水账")]
public class DayAccount 
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [Required]
    [SugarColumn(ColumnName = "Id", IsIdentity = false, ColumnDescription = "主键Id", IsPrimaryKey = true)]
    public long Id { get; set; }
    
    /// <summary>
    /// 日期
    /// </summary>
    [SugarColumn(ColumnName = "NowTime", ColumnDescription = "日期")]
    public DateTime? NowTime { get; set; }
    
    /// <summary>
    /// 金额
    /// </summary>
    [SugarColumn(ColumnName = "Money", ColumnDescription = "金额", Length = 8, DecimalDigits=4 )]
    public decimal? Money { get; set; }
    
    /// <summary>
    /// 支付方式
    /// </summary>
    [SugarColumn(ColumnName = "PayType", ColumnDescription = "支付方式", Length = 32)]
    public string? PayType { get; set; }
    
    /// <summary>
    /// 收入支出
    /// </summary>
    [SugarColumn(ColumnName = "InOrOut", ColumnDescription = "收入支出", Length = 32)]
    public string? InOrOut { get; set; }
    
    /// <summary>
    /// 商品
    /// </summary>
    [SugarColumn(ColumnName = "Commodity", ColumnDescription = "商品", Length = 32)]
    public string? Commodity { get; set; }
    
}
