﻿using Admin.NET.Core;
namespace Admin.NET.Application.Entity;

/// <summary>
/// 测试
/// </summary>
[SugarTable("TestTab","测试")]
public class TestTab  : EntityBase
{
    /// <summary>
    /// TA
    /// </summary>
    [SugarColumn(ColumnName = "TAvar", ColumnDescription = "TA", Length = 32)]
    public string? TAvar { get; set; }
    
    /// <summary>
    /// TB
    /// </summary>
    [SugarColumn(ColumnName = "TBchar", ColumnDescription = "TB", Length = 32)]
    public string? TBchar { get; set; }
    
    /// <summary>
    /// TC
    /// </summary>
    [SugarColumn(ColumnName = "TCint", ColumnDescription = "TC")]
    public int? TCint { get; set; }
    
    /// <summary>
    /// TDdatetime
    /// </summary>
    [SugarColumn(ColumnName = "TDdatetime", ColumnDescription = "TDdatetime")]
    public DateTime? TDdatetime { get; set; }
    
    /// <summary>
    /// TEtimestamp
    /// </summary>
    [SugarColumn(ColumnName = "TEtimestamp", ColumnDescription = "TEtimestamp", Length = 8)]
    public string? TEtimestamp { get; set; }
    
    /// <summary>
    /// 租户Id
    /// </summary>
    [SugarColumn(ColumnName = "TenantId", ColumnDescription = "租户Id")]
    public long? TenantId { get; set; }
    
    /// <summary>
    /// 创建者姓名
    /// </summary>
    [SugarColumn(ColumnName = "CreateUserName", ColumnDescription = "创建者姓名", Length = 15)]
    public string? CreateUserName { get; set; }
    
}
