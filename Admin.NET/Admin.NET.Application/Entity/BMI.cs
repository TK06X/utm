﻿namespace Admin.NET.Application.Entity;

/// <summary>
/// 身体质量指数（Body Mass Index）
/// </summary>
[SugarTable("BMI", "身体质量指数（Body Mass Index）")]
public class BMI  
{
    /// <summary>
    /// 主键Id
    /// </summary>
    [Required]
    [SugarColumn(ColumnName = "Id", IsIdentity = false, ColumnDescription = "主键Id", IsPrimaryKey = true)]
    public long Id { get; set; }

    /// <summary>
    /// 身高
    /// </summary>
    [SugarColumn(ColumnName = "Height", ColumnDescription = "身高", Length = 8, DecimalDigits = 4)]
    public decimal? Height { get; set; }

    /// <summary>
    /// 体重
    /// </summary>
    [SugarColumn(ColumnName = "Weight", ColumnDescription = "体重", Length = 8, DecimalDigits = 4)]
    public decimal? Weight { get; set; }

    /// <summary>
    /// BMI
    /// </summary>
    [SugarColumn(ColumnName = "BmiComp", ColumnDescription = "BMI", Length = 8, DecimalDigits = 4)]
    public decimal? BmiComp { get; set; }

    /// <summary>
    /// 创建时间
    /// </summary>
    [SugarColumn(ColumnName = "CreateTime", ColumnDescription = "创建时间")]
    public DateTime? CreateTime { get; set; }

    /// <summary>
    /// 更新时间
    /// </summary>
    [SugarColumn(ColumnName = "UpdateTime", ColumnDescription = "更新时间")]
    public DateTime? UpdateTime { get; set; }

    /// <summary>
    /// 软删除
    /// </summary>
    [Required]
    [SugarColumn(ColumnName = "IsDelete", ColumnDescription = "软删除")]
    public bool IsDelete { get; set; }

    /// <summary>
    /// 记录时间
    /// </summary>
    [Required]
    [SugarColumn(ColumnName = "NowTime", ColumnDescription = "记录时间")]
    public DateTime NowTime { get; set; }
}