# 1 后端

数据库配置

```json
  {
            "DbType": "SqlServer",
            "ConnectionString": "Server=CFL;Database=Read_ENT;User Id = sa;Password=123456;Trusted_Connection=True;TrustServerCertificate=True", // 库连接字符串
            "EnableInitDb": false, // 启用库表初始化
            "EnableInitSeed": true, // 启用种子初始化
            "EnableDiffLog": true, // 启用库表差异日志
            "EnableUnderLine": false // 启用驼峰转下划线
          }
```

# 2 业务功能

## 2.1 记录

### 不继承基类的某个字段

`[NotMapped]` 属性是 Entity Framework 提供的一种方式，用于告诉 Entity Framework 忽略某个属性或类的映射。

```c#
using System.ComponentModel.DataAnnotations.Schema;

public class A_SleepRecord : EntityBase  
{  
    [NotMapped]  
    public string Comment   
    {   
        get;   
        set;   
    }  
}
```

###  sleepRecord实际开发配置

​		 namespace Admin.NET.Application 命名空间改名 `namespace Admin.NET.Application.Service;` 

### TimeSpan 数据类型

代表时，分，秒

### 枚举

```C#
public enum SleepStateEnum
{
    /// <summary>
    /// 良好
    /// </summary>
    [Description("良好")]
    Fine = 3
}
input.State = SleepStateEnum.Fine;
```

### 必填 功能

```c#
/// <summary>
/// TB
/// </summary>
[Required(ErrorMessage = "DateTime不能为空")]
public override DateTime? NowTime { get; set; }
```

### 接口 查找

```c#
 var query = _rep.AsQueryable()
            .WhereIF(input.Duration.HasValue, u => u.Duration.Value.Hours == input.Duration)
            .WhereIF(input.State.HasValue,u=>u.State==input.State)
            .Where(a => a.IsDelete == false)
            .Select<SleepRecord>();

 var query =   _rep.AsQueryable()
            .WhereIF(input.Year.HasValue, u => u.StartTime.Value.Year == input.Year)
            .WhereIF(input.Month.HasValue, u => u.StartTime.Value.Month == input.Month)
            .Where(a => a.IsDelete == false)
            .Select<MonthSearchOutPut>();

```

- 增加年月

​	 

### 查询结果排序

```C# 
/// <summary>
/// 分页查询SleepRecord
/// </summary>
/// <param name="input"></param>d
/// <returns></returns>
[HttpPost]
[ApiDescriptionSettings(Name = "Page")]
public async Task<SqlSugarPagedList<SleepRecord>> Page(SleepRecordInput input)
{
    var query = _rep.AsQueryable()
        .WhereIF(input.State.HasValue, u => u.State == input.State)
        .WhereIF(input.Year.HasValue, u => u.StartTime.Value.Year == input.Year)
        .WhereIF(input.Month.HasValue, u => u.StartTime.Value.Month == input.Month)
         .OrderBy(s=>s.StartTime,OrderByType.Asc)
        .Where(a => a.IsDelete == false)
        .Select<SleepRecord>();

    query = query.OrderBuilder(input, "", "StartTime");
    return await query.ToPagedListAsync(input.Page, input.PageSize);
}
//    query = query.OrderBuilder(input, "", "StartTime");
三个参数 第三个参数是正序还是倒序查询
```

### 接口校验 是否存在

```c#
 var isExist = await _rep.AsQueryable()
            .Filter(null, true)
            .AnyAsync(u => u.NowTime == input.NowTime);

if (isExist) throw Oops.Oh("该日期已存在:" + input.NowTime);

```



###  假删除 跟 真删除

```js
 await _rep.FakeDeleteAsync(entity); //假删除
        //await _rep.DeleteAsync(entity);   //真删除
```



### 私有方法

```js
  private SleepRecordBaseInput CalculateTime(SleepRecordBaseInput input)
    {
    }
```


### 简单排序

```js

.OrderBy(s=>s.StartTime,OrderByType.Asc)
 https://www.donet5.com/Home/Doc?typeId=2312    排序 OrderBy

```



###
